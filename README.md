# Windows Activation

A repository of batch scripts I made that simplify Windows and Windows products activation via a local KMS server emulator.

These scripts mostly provide an easy way to automate a lot of tasks. For the activation itself, I use [KMS_VL_ALL_AIO](https://codepen.io/abbodi1406/full/yLaYNKr).

### activation-aio.cmd

This is an activation all-in-one script. It makes sure it's ran as Administrator, downloads the activation script (while temporarely modifying Windows Defender) and runs it.

### office-activation-aio.cmd

This is an Office 2019 all-in-one script. It installs Office 2019 via the Office Deployment Tool (installation this way fixes '[Your license isn't genuine](https://infohost.github.io/office-license-is-not-genuine)' errors) and then does the same thing as activation-aio.cmd.

## Acknowledgements

[abbodi1406](https://forums.mydigitallife.net/threads/abbodi1406s-batch-scripts-repo.74197/#post-1343297) for creating KMS_VL_ALL and [KMS_VL_ALL_AIO](https://codepen.io/abbodi1406/full/yLaYNKr)

[WindowsAddict](https://forums.mydigitallife.net/members/1108726/) for information about fixing '[Your license isn't genuine](https://infohost.github.io/office-license-is-not-genuine)' error (since I learned about installing Office with the Office Deployment Tool from him).
