@echo off
:: Get admin priviledges
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

set "download=bitsadmin /transfer Downloading /download /priority foreground"
set "office_config=Configuration.xml"

cd /d C:\

:: ===================
:: Office installation
:: ===================

(
echo ^<Configuration ID="78d1981f-a275-4621-92a5-95718b5d0931"^>
echo   ^<Add OfficeClientEdition="64" Channel="PerpetualVL2019"^>
echo     ^<Product ID="Standard2019Volume" PIDKEY="6NWWJ-YQWMR-QKGCB-6TMB3-9D9HK"^>
echo       ^<Language ID="en-us" /^>
echo       ^<ExcludeApp ID="Groove" /^>
echo       ^<ExcludeApp ID="OneDrive" /^>
echo       ^<ExcludeApp ID="OneNote" /^>
echo       ^<ExcludeApp ID="Outlook" /^>
echo       ^<ExcludeApp ID="Publisher" /^>
echo     ^</Product^>
echo   ^</Add^>
echo   ^<Property Name="SharedComputerLicensing" Value="0" /^>
echo   ^<Property Name="SCLCacheOverride" Value="0" /^>
echo   ^<Property Name="AUTOACTIVATE" Value="0" /^>
echo   ^<Property Name="FORCEAPPSHUTDOWN" Value="FALSE" /^>
echo   ^<Property Name="DeviceBasedLicensing" Value="0" /^>
echo   ^<Updates Enabled="TRUE" /^>
echo   ^<RemoveMSI /^>
echo   ^<Display Level="Full" AcceptEULA="TRUE" /^>
echo ^</Configuration^>
)>%office_config%

%download% https://officecdn.microsoft.com/pr/wsus/setup.exe %cd%\setup.exe

%cd%setup.exe /configure Configuration.xml

del %office_config%
del %cd%setup.exe

:: ==========================
:: KMS Activator Installation
:: ==========================

:: Add exception to Windows Defender
powershell -inputformat none -NonInteractive -Command Add-MpPreference -ExclusionProcess %cd%KMS_VL_ALL_AIO.cmd -Force
powershell -inputformat none -NonInteractive -Command Add-MpPreference -ExclusionExtension ".tmp" -Force

%download% https://raw.githubusercontent.com/abbodi1406/KMS_VL_ALL_AIO/master/KMS_VL_ALL_AIO.cmd %cd%KMS_VL_ALL_AIO.cmd

%cd%KMS_VL_ALL_AIO.cmd /a

del %cd%KMS_VL_ALL_AIO.cmd

:: Remove exception from Windows Defender
powershell -inputformat none -NonInteractive -Command Remove-MpPreference -ExclusionProcess %cd%KMS_VL_ALL_AIO.cmd -Force
powershell -inputformat none -NonInteractive -Command Remove-MpPreference -ExclusionExtension ".tmp" -Force

echo Done
PAUSE
