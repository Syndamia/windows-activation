@echo off
:: Get admin priviledges
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

set "download=bitsadmin /transfer Downloading /download /priority foreground"

cd /d C:\

:: ==========================
:: KMS Activator Installation
:: ==========================

:: Add exception to Windows Defender
powershell -inputformat none -NonInteractive -Command Add-MpPreference -ExclusionProcess %cd%KMS_VL_ALL_AIO.cmd -Force
powershell -inputformat none -NonInteractive -Command Add-MpPreference -ExclusionExtension ".tmp" -Force

%download% https://raw.githubusercontent.com/abbodi1406/KMS_VL_ALL_AIO/master/KMS_VL_ALL_AIO.cmd %cd%KMS_VL_ALL_AIO.cmd

%cd%KMS_VL_ALL_AIO.cmd /a

del %cd%KMS_VL_ALL_AIO.cmd

:: Remove exception from Windows Defender
powershell -inputformat none -NonInteractive -Command Remove-MpPreference -ExclusionProcess %cd%KMS_VL_ALL_AIO.cmd -Force
powershell -inputformat none -NonInteractive -Command Remove-MpPreference -ExclusionExtension ".tmp" -Force

echo Done
PAUSE
